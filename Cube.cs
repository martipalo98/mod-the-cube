﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{
    public MeshRenderer Renderer;
    public Camera camera;
    public Color sceneBackgroundColor;
    public Vector3 position;
    public float scale;
    public  Material material;
    public Color color;
    public Vector3 rotationAngle;
    public float rotationSpeed;
    
    void Start()
    {
        position = new Vector3(3, 4, 1);
        scale = Random.Range(0.0f, 3.0f);
        color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        rotationAngle = new Vector3(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        rotationSpeed = Random.Range(1f, 100f);
        material = Renderer.material;
        sceneBackgroundColor = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f)); // Set the background color of the main camera


    }
    
    void Update()
    {
        transform.position = position;
        transform.localScale = Vector3.one * scale;
        material.color = color;
        transform.Rotate(Time.deltaTime * rotationSpeed * rotationAngle.x, Time.deltaTime * rotationSpeed * rotationAngle.y, Time.deltaTime * rotationSpeed * rotationAngle.z);
        camera.backgroundColor = sceneBackgroundColor;
    }
}
